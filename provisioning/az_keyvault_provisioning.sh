#!/bin/zsh

## Log in to Azure
az login

## Set subscription
az account set --subscription $AZURE_SUBSCRIPTION_ID

## Register Key Vault as a provider
az provider register -n Microsoft.KeyVault

## Assign extended Secrets Access Policies to the Cloud Ops AD group
az keyvault set-policy --name keyvault-bgdata --object-id $AZURE_AD_CLOUDOPS_GROUP_ID --secret-permissions get list set backup restore recover

## get the Azure function app Service Principal details
APP_PRINCIPAL=$(eval az ad sp list --display-name $AZURE_APP_NAME | jq '.[].objectId?')
APP_SLOT_PRINCIPAL=$(eval az ad sp list --display-name "$AZURE_APP_NAME/slots/dev" | jq '.[].objectId?')
APP_PRINCIPAL=$(echo "$APP_PRINCIPAL" | tr -d '"')

## Assign get and list Secret Access Policies to the bgspyder apps principals
PRINCIPALS=(
# $AZURE_BGDATA_OBJECT_ID
$APP_PRINCIPAL
$APP_SLOT_PRINCIPAL
)

for principal in $PRINCIPALS
do
    echo $principal
    az keyvault set-policy --name keyvault-bgdata --object-id $principal --secret-permissions get list
done

## Restore secrets in Key Vault
for filepath in local_only/kv_secrets/*
do 
    az keyvault secret restore --file $filepath --vault-name keyvault-bgdata --subscription 1f7d3648-a477-4f53-9f57-f6e67aaf5686
done