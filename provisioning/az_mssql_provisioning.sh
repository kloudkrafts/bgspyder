#!/bin/zsh

## Log in to Azure
az login

## Set subscription
az account set --subscription $AZURE_SUBSCRIPTION_ID

## Create a firewall rule allowing any Azure resource to access the MSSQL server
az sql server firewall-rule create -s bgdata -g rg-bgdata -n AllowAllAzureResources --end-ip-address 0.0.0.0 --start-ip-address 0.0.0.0