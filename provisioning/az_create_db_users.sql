CREATE SCHEMA prestashop
GO

CREATE SCHEMA odoo
GO

CREATE USER [bgspyder] FROM EXTERNAL PROVIDER;
GRANT CONTROL ON SCHEMA::odoo TO [bgspyder];
GRANT CONTROL ON SCHEMA::prestashop TO [bgspyder];

CREATE USER [loan.beloeil@bonnegueule.fr] FROM EXTERNAL PROVIDER;
GRANT CONTROL ON SCHEMA::odoo TO [loan.beloeil@bonnegueule.fr];
GRANT CONTROL ON SCHEMA::prestashop TO [loan.beloeil@bonnegueule.fr];