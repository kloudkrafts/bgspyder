INSERT INTO odoo.Clients(ClientId, TotalInvoiced, TotalOrders, ZipCode, City, Region, Country, LoyaltyPoints)
    VALUES (1, 666.0, 1, 75002, 'Paris', 'IDF', 'France', 10);

INSERT INTO odoo.Products(ProductId, ProductName, EAN, SalesCount, ListPrice, IsVariant, Size)
    VALUES (934, 'Manteau Gall Bleu', '3701020201671', 42, 420.0, 1, '46');

INSERT INTO odoo.Products(ProductId, ProductName, EAN, SalesCount, ListPrice, IsVariant, Size)
    VALUES (123, 'Produit mystère', '3701020266671', 33, 112.8, 1, 'M');

INSERT INTO odoo.Orders(OrderId, ClientId, OrderRef, PrestaShopId, PrestaShopRef, DateOrder, AmountHT, AmountTax, OrderState, InvoiceStatus)
    VALUES (876, 1, 'SO0876', 2987, 'PRESTAXV35789', '20200329 21:34:12', 532.8, 133.2, 'paid', 'paid');

INSERT INTO odoo.OrderLines(OrderLineId, OrderId, DisplayName, ProductId, ProductQty, UnitPriceHT, UnitPriceTax)
    VALUES (5678, 876, 'Manteau Gall Bleu T:46', 934, 1, 420.0, 105.0);

INSERT INTO odoo.OrderLines(OrderLineId, OrderId, DisplayName, ProductId, ProductQty, UnitPriceHT, UnitPriceTax)
    VALUES (5679, 876, 'Produit mystère T:M', 123, 1, 112.8, 28.2);

INSERT INTO odoo.Invoices(InvoiceId, OrderId, ClientId, DateInvoice, AmountHT, AmountTax, AcctJournal)
    VALUES (777, 876, 1, '20200329 21:34:12', 532.8, 133.2, '513442590');