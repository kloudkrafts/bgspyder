-- DROP TABLE odoo.PosOrderLines;
DROP TABLE odoo.PosOrders;
-- DROP TABLE odoo.WebOrderLines;
-- DROP TABLE odoo.WebOrders;
-- DROP TABLE odoo.Products;
-- DROP TABLE odoo.ProductsPurchase;
-- DROP TABLE odoo.Clients;
-- DROP TABLE odoo.AccountingPieces;
-- DROP TABLE odoo.AccountingLines;
-- DROP TABLE odoo.Transfers;
-- DROP TABLE odoo.WebReturns;
-- DROP TABLE odoo.WebReturnLines;
-- DROP TABLE odoo.Countries;
-- DROP TABLE odoo.ProductDefect;
-- DROP SCHEMA odoo;


-- DROP TABLE prestashop.WebOrders;
-- DROP TABLE prestashop.WebOrderLines;
-- DROP TABLE prestashop.Countries;
-- DROP TABLE prestashop.CustomerGroups;
-- DROP TABLE prestashop.Clients;
-- DROP TABLE prestashop.WebReturns;
-- DROP TABLE prestashop.WebReturnLines;
-- DROP TABLE prestashop.Addresses;
-- DROP TABLE prestashop.Carriers;
-- DROP SCHEMA prestashop;