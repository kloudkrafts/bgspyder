
CREATE TABLE odoo.Products(
    ProductId int PRIMARY KEY NOT NULL,
    ProductName varchar(35),
    EAN int,
    SalesCount int,
    ListPrice money,
    IsVariant boolean,
    Size varchar(10)
)
GO

CREATE TABLE odoo.Clients(
    ClientId int PRIMARY KEY NOT NULL,
    TotalInvoiced money,
    TotalOrders int,
    Zipcode varchar(10),
    City varchar(100),
    Region varchar(100),
    Country varchar(100),
    LoyaltyPoints int
)
GO

CREATE TABLE odoo.Orders(
    OrderId int PRIMARY KEY NOT NULL,
    ClientId int,
    OrderRef varchar(25) NOT NULL,
    PrestaShopId int,
    PrestaShopRef varchar(25),
    DateOrder datetime NOT NULL,
    AmountHT money NOT NULL,
    AmountTax money NOT NULL,
    OrderState varchar(25),
    InvoiceStatus varchar(25),
    CONSTRAINT FK_Orders_Clients FOREIGN KEY (ClientId) REFERENCES odoo.Clients (ClientId)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
GO

CREATE TABLE odoo.OrderLines(
    OrderLineId int PRIMARY KEY NOT NULL,
    OrderId int,
    DisplayName varchar(100),
    ProductId int,
    ProductQty int,
    UnitPriceHT money,
    UnitPriceTax money,
    CONSTRAINT FK_OrderLine_Order FOREIGN KEY (OrderId) REFERENCES odoo.Orders (OrderId),
    CONSTRAINT FK_OrderLine_Product FOREIGN KEY (ProductId) REFERENCES odoo.Products (ProductId)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
GO

CREATE TABLE odoo.Invoices(
    InvoiceId int PRIMARY KEY NOT NULL,
    OrderId int,
    ClientId int,
    RefundInvoiceId int,
    DateInvoice datetime NOT NULL,
    AmountHT money NOT NULL,
    AmountTax money NOT NULL,
    CONSTRAINT FK_Invoice_Order FOREIGN KEY (OrderId) REFERENCES odoo.Orders (OrderId),
    CONSTRAINT FK_Invoices_Clients FOREIGN KEY (ClientId) REFERENCES odoo.Clients (ClientId),
    CONSTRAINT FK_Invoice_Refund FOREIGN KEY (RefundInvoiceId) REFERENCES odoo.Invoices (InvoiceId)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
GO

INSERT INTO 