-- DBCC FREESYSTEMCACHE ( 'ALL' ) WITH NO_INFOMSGS;
-- GO
-- SELECT * FROM odoo.WebOrderLines;
SELECT * FROM odoo.WebOrders ORDER BY DateOrder ASC;
-- SELECT * FROM odoo.PosOrderLines ORDER BY Id DESC;
-- SELECT * FROM odoo.PosOrders ORDER BY Id DESC;
-- SELECT * FROM odoo.WebReturns ORDER BY CreatedAt DESC;
-- SELECT * FROM odoo.WebReturnLines;
-- SELECT * FROM odoo.AccountingPieces ORDER BY CreatedAt DESC;
-- SELECT * FROM odoo.AccountingLines ORDER BY Id DESC;
-- SELECT * FROM odoo.Clients ORDER BY Id DESC;
-- SELECT * FROM odoo.CustomerChurn;
-- SELECT * FROM odoo.Clients_Extended WHERE TotalWebOrders > 3 ORDER BY Id DESC;
-- SELECT * FROM odoo.Products ORDER BY LastModified DESC;
-- SELECT * FROM odoo.Products WHERE LogisticCategory is not Null ORDER BY Id DESC;
-- SELECT * FROM odoo.ProductsPurchase;
-- SELECT * FROM odoo.Transfers ORDER BY TransferId DESC;
-- SELECT * FROM odoo.ProductDefect;
-- SELECT * FROM odoo.Countries;
-- SELECT * FROM odoo.AcctJournals;

-- SELECT * FROM odoo.Clients_Extended;

-- SELECT COUNT(*) FROM odoo.AccountingPieces;
-- SELECT COUNT(*) FROM odoo.AccountingLines;
-- SELECT COUNT(*) FROM odoo.WebOrders;
-- SELECT COUNT(*) FROM odoo.WebOrderLines;
-- SELECT COUNT(*) FROM odoo.Transfers;


-- SELECT * FROM prestashop.WebOrders;
-- SELECT * FROM prestashop.WebOrders_Extended;
-- SELECT * FROM prestashop.Churn;
-- SELECT * FROM prestashop.WebOrderLines;
-- SELECT * FROM prestashop.WebOrderLines_Extended;
-- SELECT * FROM prestashop.Clients;
-- SELECT * FROM prestashop.Clients_Extended;
-- SELECT * FROM prestashop.Carriers;
-- SELECT * FROM prestashop.Addresses;
-- SELECT * FROM prestashop.CustomerGroups;
-- SELECT * FROM prestashop.Countries;
-- SELECT * FROM prestashop.WebOrderLines ORDER BY WebOrderLineId DESC;