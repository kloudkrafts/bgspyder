-- CREATE SCHEMA odoo
-- GO

CREATE TABLE odoo.Products(
    Id int PRIMARY KEY NOT NULL,
    ProductTemplateId int,
    ProductName varchar(100),
    EAN varchar(13),
    CreatedAt datetime,
    LastModified datetime,
    SalesCount int,
    ListPrice money,
    IsVariant bit,
    Size varchar(10),
    ProductCategoryId int,
    ProductCategory varchar(100),
    LogisticCategoryId int,
    LogisticCategory varchar(100),
    Season varchar(100),
    OriginalReleaseDate datetime
)
GO

CREATE TABLE odoo.ProductsPurchase(
    Id int PRIMARY KEY NOT NULL,
    Supplier varchar(100),
    ProductTemplateId int,
    PurchasePrice money
)
GO

CREATE TABLE odoo.Clients(
    Id int PRIMARY KEY NOT NULL,
    PrestaShopId int,
    CreatedAt datetime,
    CreatedById int,
    CreatedBy varchar(100),
    TotalInvoiced money,
    TotalWebOrders int,
    TotalPosOrders int,
    TotalOrders int,
    Zipcode varchar(10),
    City varchar(100),
    CountryId int,
    Country varchar(255),
    Gender int,
    LoyaltyPoints int
)
GO

CREATE TABLE odoo.Transfers(
    Id int PRIMARY KEY NOT NULL,
    Ref varchar(50),
    CreatedAt datetime,
    CreatedById int,
    TransferDate datetime,
    CompletedDate datetime,
    OrderRef varchar(50),
    OriginId int,
    Origin varchar(100),
    DestinationId int,
    Destination varchar(100),
    PickingTypeCode varchar(50),
    PickingTypeId int,
    PickingType varchar(100),
    CarrierId int,
    Carrier varchar(100),
    ProductId int,
    ProductName varchar(255)
)
GO

CREATE TABLE odoo.WebReturns(
    Id int PRIMARY KEY NOT NULL,
    CreatedAt datetime,
    OrderId int,
    OrderRef varchar(50),
    ReturnState varchar(50)
)
GO

CREATE TABLE odoo.WebReturnLines(
    Id int PRIMARY KEY NOT NULL,
    ReturnId int,
    ReasonId int,
    Reason varchar(255),
    ProductId int,
    ProductName varchar(255),
    ReturnedQty int
)
GO

CREATE TABLE odoo.WebOrders(
    Id int PRIMARY KEY NOT NULL,
    ClientId int,
    OrderRef varchar(25) NOT NULL,
    PrestaShopId int,
    PrestaShopRef varchar(25),
    DateOrder datetime NOT NULL,
    AmountHT money NOT NULL,
    AmountTax money NOT NULL,
    OrderState varchar(25),
    InvoiceStatus varchar(25)
    -- CONSTRAINT FK_Orders_Clients FOREIGN KEY (ClientId) REFERENCES odoo.Clients (ClientId)
    -- ON DELETE CASCADE
    -- ON UPDATE CASCADE
)
GO

CREATE TABLE odoo.WebOrderLines(
    Id int PRIMARY KEY NOT NULL,
    WebOrderId int,
    OrderRef varchar(100),
    DisplayName varchar(100),
    ProductId int,
    ProductName varchar(255),
    ProductQty int,
    UnitPriceTTC money
    -- CONSTRAINT FK_OrderLine_Order FOREIGN KEY (OrderId) REFERENCES odoo.Orders (OrderId),
    -- CONSTRAINT FK_OrderLine_Product FOREIGN KEY (ProductId) REFERENCES odoo.Products (ProductId)
    -- ON DELETE CASCADE
    -- ON UPDATE CASCADE
)
GO

CREATE TABLE odoo.PosOrders(
    Id int PRIMARY KEY NOT NULL,
    ReceiptId varchar(25),
    OrderRef varchar(100) NOT NULL,
    DateOrder datetime NOT NULL,
    AmountPaid money,
    AmountReturned money,
    AmountTTC money NOT NULL,
    AmountTax money NOT NULL,
    OrderState varchar(25),
    InvoiceStatus varchar(25),
    ReturnRef varchar(25),
    ReturnStatus varchar(25)
    -- CONSTRAINT FK_Orders_Clients FOREIGN KEY (ClientId) REFERENCES odoo.Clients (ClientId)
    -- ON DELETE CASCADE
    -- ON UPDATE CASCADE
)
GO

CREATE TABLE odoo.PosOrderLines(
    Id int PRIMARY KEY NOT NULL,
    PosOrderId int,
    OrderRef varchar(50),
    ProductId int,
    ProductName varchar(255),
    UnitPriceTTC money NOT NULL,
    ProductQty int,
    ReturnedQty int,
    AmountHT money NOT NULL
    -- CONSTRAINT FK_OrderLine_Order FOREIGN KEY (OrderId) REFERENCES odoo.Orders (OrderId),
    -- CONSTRAINT FK_OrderLine_Product FOREIGN KEY (ProductId) REFERENCES odoo.Products (ProductId)
    -- ON DELETE CASCADE
    -- ON UPDATE CASCADE
)
GO

CREATE TABLE odoo.AccountingPieces(
    Id int PRIMARY KEY NOT NULL,
    OrderId int NOT NULL,
    OrderRef varchar(50),
    PcNumber varchar(25),
    Ref varchar(50),
    PcDate datetime,
    PcState varchar(25),
    PcType varchar(25),
    ClientId int,
    CreatedAt datetime NOT NULL,
    InvoiceDate datetime NOT NULL,
    AmountHT money NOT NULL,
    Taxes money NOT NULL,
    AmountTTC money NOT NULL
)
GO

CREATE TABLE odoo.AccountingLines(
    Id int PRIMARY KEY NOT NULL,
    AcctPieceId int,
    AcctLineName varchar(255),
    LineDate datetime,
    DisplayName varchar(255),
    InvoiceId int,
    InvoiceNo varchar(255),
    AccountType varchar(25),
    AccountId int,
    Account varchar(255),
    ExcludeFromInvoice bit,
    ClientId int,
    InvoiceRef varchar(255),
    ProductId int,
    ProductName varchar(255),
    UnitPriceTTC money,
    ProductQty int,
    AmountHT money NOT NULL,
    Taxes money NOT NULL,
    AmountTTC money NOT NULL
)
GO

CREATE TABLE odoo.Countries(
    Id int PRIMARY KEY NOT NULL,
    CountryName varchar(255),
    CountryCode varchar(2)
)
GO

CREATE TABLE odoo.ProductDefect(
    Id int PRIMARY KEY NOT NULL,
    DefectName varchar(255),
    CreatedAt datetime,
    LastModified datetime
)