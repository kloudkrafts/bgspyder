-- CREATE SCHEMA prestashop
-- GO

CREATE TABLE prestashop.WebOrders(
    WebOrderId int PRIMARY KEY NOT NULL,
    ClientId int,
    OrderRef varchar(25) NOT NULL,
    DateOrder datetime NOT NULL,
    AmountHT money NOT NULL,
    AmountTTC money NOT NULL,
    OrderState varchar(25),
    InvoiceRef varchar(25),
    DeliveryAddressId int,
    InvoicingAddressId int,
    CarrierId int
)
GO

CREATE TABLE prestashop.WebOrderLines(
    WebOrderLineId int PRIMARY KEY NOT NULL,
    WebOrderId int,
    ProductId int,
    ProductName varchar(100),
    EAN varchar(13),
    ProductQty int,
    ReturnedQty int,
    UnitPriceHT money NOT NULL,
    UnitPriceTTC money NOT NULL,
    TotalPriceHT money NOT NULL,
    TotalPriceTTC money NOT NULL
)
GO

CREATE TABLE prestashop.Clients(
    ClientId int PRIMARY KEY NOT NULL,
    CreatedAt datetime,
    LastUpdated datetime,
    GenderId int,
    DefaultGroupId int,
    Birthdate varchar(10)
)
GO

CREATE TABLE prestashop.CustomerGroups(
    GroupId int,
    Lang int,
    GroupName varchar(100)
)
GO

CREATE TABLE prestashop.WebReturns(
    ReturnId int PRIMARY KEY NOT NULL,
    ClientId int,
    WebOrderId int,
    ReturnState varchar(50),
    CreatedAt datetime,
    LastUpdated datetime
)
GO

CREATE TABLE prestashop.WebReturnLines(
    WebOrderLineId int,
    ReturnId int,
    ReturnedQty int
)
GO

CREATE TABLE prestashop.Addresses(
    AddressId int PRIMARY KEY NOT NULL,
    ClientId int,
    CountryId int,
    City varchar(255),
    ZipCode varchar(7),
    CreatedAt datetime,
    LastUpdated datetime,
    IsActive bit,
    IsDeleted bit
)
GO

CREATE TABLE prestashop.Countries(
    CountryId int,
    Lang int,
    CountryName varchar(255)
)
GO

CREATE TABLE prestashop.Carriers(
    CarrierId int PRIMARY KEY NOT NULL,
    CarrierName varchar(255),
    IsActive bit,
    IsDeleted bit
)
GO